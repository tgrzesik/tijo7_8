package pl.edu.pwsztar.domain.files.filesImpl;

import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.dto.GeneratedFileDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.files.TextFileGenerator;

import java.io.*;
import java.util.Date;

@Component
public class TextFileGeneratorImpl implements TextFileGenerator {

    private static final String FILE_NAME_PREFIX = "test_";
    private static final String FILE_EXTENSION = ".txt";
    private static final String FILE_CONTENT_SEPARATOR = " ";

    @Override
    public GeneratedFileDto toTxt(FileDto fileDto) throws IOException {
        File file = writeToFile(fileDto);

        InputStream stream = new FileInputStream(file);
        InputStreamResource inputStreamResource = new InputStreamResource(stream);

        return new GeneratedFileDto.Builder()
                .resource(inputStreamResource)
                .contentLength(file.length())
                .fileNameWithExtension(getFileName() + FILE_EXTENSION)
                .build();
    }

    private File writeToFile(FileDto fileDto) throws IOException {
        File file = File.createTempFile("tmp", FILE_EXTENSION);
        FileOutputStream fos = new FileOutputStream(file);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        for (MovieDto movie : fileDto.getMovies()) {
            bw.write(movie.getYear() + FILE_CONTENT_SEPARATOR + movie.getTitle());
            bw.newLine();
        }

        bw.close();
        fos.flush();
        fos.close();

        return file;
    }

    private String getFileName() {
        return FILE_NAME_PREFIX + (new Date().getTime());
    }
}
